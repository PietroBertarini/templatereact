import api from '@/store/api';

export interface CreateButtonRequest {
  year: number,
  month: number
}

export interface IApiButton {
  id: number,

  /**
   * .NET Date string. i.e: "2020-11-03T00:00:00"
   */
  buttonDate: string,

  userId: number,
}

export async function createButton(request: CreateButtonRequest) : Promise<IApiButton | IApiError> {
  try {
    const result = await api.post<IApiButton>('Button', request, {
      withCredentials: true,
    });

    return result.data;
  } catch (errorFromApi) {
    return constructErrorFromApi(errorFromApi);
  }
}

export async function deleteButton(buttonId: number) : Promise<boolean | IApiError> {
  try {
    const result = await api.delete(`Button/${buttonId}`, {
      withCredentials: true,
    });

    return result.data;
  } catch (errorFromApi) {
    return constructErrorFromApi(errorFromApi);
  }
}
