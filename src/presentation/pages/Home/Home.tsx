import React from "react";
import {Badge} from "@/presentation/compoments";
import { Body } from "./components";

export const Home = () => {
  return (
    <div>
      <Badge />
      <Body />
    </div>
  );
};

