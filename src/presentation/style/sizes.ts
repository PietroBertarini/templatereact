/**
 * Font Size
 */

export const LogoFontSize = '30px';
export const HeaderFontSize = '22px';
export const RegularFontSize = '14px';
