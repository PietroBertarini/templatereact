import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`        
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
    font-family: Poppins,serif;
    letter-spacing: 0;
    font-weight: bold;
  }
  button {
    cursor: pointer;
    padding: 0;
    border: none;
    text-align: center;
    font-size: 14px/21px;
    background-color: transparent;
  }

`;
