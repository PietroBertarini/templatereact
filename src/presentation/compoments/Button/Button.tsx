import { HTMLProps } from 'react';

import { Container } from './Button.styles';

interface ButtonProps extends HTMLProps<HTMLButtonElement> {
  children: string;
}

export const Button = ({ children, onClick, disabled }: ButtonProps) => {
  return (
    <Container onClick={onClick} disabled={disabled}>
      {children}
    </Container>
  );
};

