describe('User Model tests', () => {
  test('isAuth test', () => {
    const result = isAuth();
    expect(result).toEqual(true);
  });

  test('fromStorage test', () => {
    const result = fromStorage({teste: "test"});
    expect(result).toEqual(undefined);
  });
});
