import React from "react";
import Body from "@/presentation/pages/Home/components/Body/Body";

test('Renders Body component and match to previous snapshot', () => {
    const component = shallow(<Body />);
    expect(component).toMatchSnapshot();
});
