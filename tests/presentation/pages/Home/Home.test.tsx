import React from "react";
import Home from "@/presentation/pagesy";

test('Renders Home component and match to previous snapshot', () => {
    const component = shallow(<Home />);
    expect(component).toMatchSnapshot();
});
