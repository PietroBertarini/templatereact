import React from "react";
import BadgeStructure from "@/presentation/compoments/Badge/components/BadgeStructure/BadgeStructure";

test('Renders BadgeStructure component and match to previous snapshot', () => {
  const component = shallow(<BadgeStructure  />);
  expect(component).toMatchSnapshot();
});
