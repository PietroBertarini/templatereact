import React from "react";
import Badge from "@/presentation/compoments";

test('Renders Badge component and match to previous snapshot', () => {
  const component = shallow(<Badge />);
  expect(component).toMatchSnapshot();
});
