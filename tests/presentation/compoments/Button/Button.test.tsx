import React from "react";
import Button from "@/presentation/compoments";

test('Renders Button component and match to previous snapshot', () => {
  const component = shallow(<Button  />);
  expect(component).toMatchSnapshot();
});
