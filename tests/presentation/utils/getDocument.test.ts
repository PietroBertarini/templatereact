import {getDocument} from "@/presentation/utils/getDocument";

describe('getDocument tests', () => {
  test('document Null', () => {
    const result = getDocument();
    expect(result).toEqual(null);
  });

});
