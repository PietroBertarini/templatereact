import {createStringMeasurer} from "@/presentation/utils/createStringMeasurer";

describe('createStringMeasurer tests', () => {
  test('String Null', () => {
    const result = createStringMeasurer('');
    expect(result).toEqual(null);
  });

  test('String has value', () => {
    const result = createStringMeasurer('test');
    expect(result).toEqual("teste1");
  });
});
