import buttonReducer, { INITIAL_STATE } from '../../../../src/Redux/Services/Button/Button.reducer';
import {
  EButtonActions,
  IButtonState,
  ButtonActionTypes,
} from '../../../../src/Redux/Services/Button/Button.types';
import User from '../../../../src/Classes/User/User.class';

describe('loading state tests', () => {
  let currState: IButtonState;

  beforeEach(() => {
    currState = INITIAL_STATE;
  });

  const dispatch = (action: ButtonActionTypes) => {
    currState = buttonReducer(currState, action);
  };

  describe('Loading commit test', () => {
    beforeEach(() => {
      dispatch({ type: EButtonActions.LOGIN_START, payload: { email: 'blob@blob.com', key: '123' } });
    });

    it('should not mutate loading state back to false', () => {
      dispatch({ type: EButtonActions.CLEAN_USER });
      dispatch({ type: EButtonActions.UPDATE_SPREADSHEET_ADM, payload: 'spd' });
      dispatch({ type: EButtonActions.CLEAN_USER_ERROR });

      expect(currState.isFetching).toBe(true);
    });

    it('should mutate loading state back to false on on get user success', () => {
      dispatch({
        type: EButtonActions.GET_USER_SUCCESS,
        payload: User.fromApiModel({
          expiration: '2020-11-16T02:17:19.5312297Z',
          subscriptionValidity: '2120-09-20T00:53:15.9495491',
          spreadsheetId: 's',
          role: 'ad',
          userName: 'UserName',
        }),
      });
      expect(currState.isFetching).toBe(false);
    });

    it('should mutate loading state back to false on on get user fail', () => {
      dispatch({
        type: EButtonActions.GET_USER_FAILURE,
        payload: 'Any',
      });
      expect(currState.isFetching).toBe(false);
    });
  });
});
